<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Terapia extends Model
{
    use HasFactory;
    protected $table = 'terapias';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'nombre_terapia',
        'descripcion_terapia',
    ];

    public function citas() {
        return $this->belongsToMany(Cita::class,'citas_has_terapias');
    }
}
