<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    use HasFactory;
    protected $table = 'citas';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'fecha_cita',
        'hora_cita',
        'paciente_id',
        'cupon_cita',
    ];

    public function paciente() {
        return $this->belongsTo(Paciente::class, 'paciente_id');
    }

    public function terapias() {
        return $this->belongsToMany(Terapia::class,'citas_has_terapias');
    }
}
