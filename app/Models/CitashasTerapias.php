<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CitashasTerapias extends Model
{
    use HasFactory;
    protected $table = 'citas_has_terapias';
    public $timestamps = false;
    protected $fillable = [
        'cita_id',
        'terapia_id',
    ];
}
