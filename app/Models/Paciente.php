<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $table = 'pacientes';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'nombres_paciente',
        'email_paciente',
        'ciudad_paciente',
        'telefono_id ',
    ];

    public function telefono() {
        return $this->belongsTo(Telefono::class);
    }

    public function citas() {
        return $this->hasMany(Cita::class);
    }
}
