<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cita;
use App\Models\Paciente;
use App\Models\Terapia;
use App\Models\CitashasTerapias;
use Illuminate\Support\Facades\Validator;

class CitaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $citas = Cita::select("*")
            ->with('paciente')
            ->with('terapias')
            ->get();
        
        return view('citas.index', compact('citas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pacientes = Paciente::all();
        $terapias = Terapia::all();
        return view('citas.create', compact('pacientes','terapias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'fecha_cita' => 'required',
            'hora_cita' => 'required',
            'paciente_id' => 'required',
        ]);

        if ($v->fails()) {
            return redirect()->back()->withErrors($v->errors())->withInput();
        } else {
            //$cita = Cita::create($request->all());
            $ruta="";
            if($request->hasFile("cupon_cita")){
                $file=$request->file("cupon_cita");
                
                $nombre = "cupon_".time().".".$file->guessExtension();
    
                $ruta = public_path("cupones/".$nombre);
                copy($file, $ruta);
            }

            $cita = new Cita;
            $cita->fecha_cita = $request->fecha_cita;
            $cita->hora_cita = $request->hora_cita;
            $cita->paciente_id = $request->paciente_id;
            $cita->cupon_cita = $nombre;
            $cita->save();

            CitashasTerapias::create(['cita_id'=>$cita->id, 'terapia_id'=>$request->terapia_id]);
            return redirect()->route('citas.index')->with('success', 'Horario creado exitosamente!!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
