<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horario_atencion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('dia_horario_atencion');
            $table->time('inicio_horario_atencion');
            $table->time('fin_horario_atencion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horario_atencion');
    }
};
