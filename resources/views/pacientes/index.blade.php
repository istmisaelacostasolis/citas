@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE HORARIOS DE ATENCIÓN</h1>
        <div>
            <a class="btn btn-success" href="{{ route('pacientes.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombres Paciente</th>
                <th>Email Paciente</th>
                <th>Ciudad Paciente</th>
                <th>Teléfono Paciente</th>
                <th>Citas</th>
                <th>Acciones</th>
            </tr>
            @foreach ($pacientes as $paciente)
                <tr>
                    <td>{{ $paciente->id }}</td>
                    <td>{{ $paciente->nombres_paciente }}</td>
                    <td>{{ $paciente->email_paciente }}</td>
                    <td>{{ $paciente->ciudad_paciente }}</td>
                    <td>{{ $paciente->telefono->operadora_tel }}</td>
                    <td>
                    @foreach ($paciente->citas as $cita)
                        <p>{{ $cita->fecha_cita }}</p>
                    @endforeach
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection