@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Horario</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('horarios.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('horarios.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Día Atención:</strong>
                        <input type="date" name="dia_horario_atencion" class="form-control"
                            placeholder="Ingrese el día de atención" value="{{ old('dia_horario_atencion') }}" required>
                        @error('dia_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Inicio de Atención:</strong>
                        <input type="time" name="inicio_horario_atencion" class="form-control"
                            value="{{ old('inicio_horario_atencion') }}" required>
                        @error('inicio_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora Fin de Atención:</strong>
                        <input type="time" name="fin_horario_atencion" class="form-control"
                            value="{{ old('fin_horario_atencion') }}" required>
                        @error('fin_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Tiene discapacidad:</strong>
                        <input type="tel" name="texto_horario_atencion" class="form-control"
                        value="{{ old('texto_horario_atencion') }}" pattern="[+][0-5]{3}[0-9]{9}">
                        @error('texto_horario_atencion')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection
