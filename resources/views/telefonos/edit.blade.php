@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Horario de Atención</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('horarios.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('horarios.update',$horario->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Día Atención:</strong>
                    <input type="date" name="dia_horario_atencion" class="form-control" value="{{ $horario->dia_horario_atencion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Inicio Atención:</strong>
                    <input type="time" name="inicio_horario_atencion" class="form-control" value="{{ $horario->inicio_horario_atencion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Fin Atención:</strong>
                    <input type="time" name="fin_horario_atencion" class="form-control" value="{{ $horario->fin_horario_atencion }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
