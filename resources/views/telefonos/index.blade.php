@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE TELÉFONOS</h1>
        <div>
            <a class="btn btn-success" href="">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Operadora</th>
                <th>Costo Minuto</th>
                <th>Paciente</th>
                <th>Acciones</th>
            </tr>
            @foreach ($telefonos as $telefono)
                <tr>
                    <td>{{ $telefono->id }}</td>
                    <td>{{ $telefono->operadora_tel }}</td>
                    <td>{{ $telefono->costo_minuto_tel }}</td>
                    <td>{{ $telefono->paciente->nombres_paciente }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection