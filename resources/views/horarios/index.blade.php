@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE HORARIOS DE ATENCIÓN</h1>
        <div>
            <a class="btn btn-success" href="{{ route('horarios.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Dia Atención</th>
                <th>Hora Inicio Atención</th>
                <th>Hora Fin Atención</th>
                <th>Acciones</th>
            </tr>
            @foreach ($horarios as $horario)
                <tr>
                    <td>{{ $horario->id }}</td>
                    <td>{{ $horario->dia_horario_atencion }}</td>
                    <td>{{ $horario->inicio_horario_atencion }}</td>
                    <td>{{ $horario->fin_horario_atencion }}</td>
                    <td><a class="btn btn-primary" href="{{ route('horarios.edit',$horario) }}">
                            <i class="fa-solid fa-pen-to-square"></i>
                        </a>
                        <form action="{{ route('horarios.destroy',$horario->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection