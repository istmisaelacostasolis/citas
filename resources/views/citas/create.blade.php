@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nueva Cita</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('citas.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('citas.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Fecha de Cita:</strong>
                        <input type="date" name="fecha_cita" class="form-control" value="{{ old('fecha_cita') }}"
                            required>
                        @error('fecha_cita')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora de Cita:</strong>
                        <input type="time" name="hora_cita" class="form-control" value="{{ old('hora_cita') }}" required>
                        @error('hora_cita')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Pacientes:</strong>
                        <select class="form-select" name="paciente_id" aria-label="Default select example">
                            <option value="undefined" selected="true" disabled>Seleccione un paciente:</option>
                            @foreach ($pacientes as $paciente)
                                <option value={{$paciente->id}}>{{$paciente->nombres_paciente}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Terapias:</strong>
                        <select class="form-select" name="terapia_id" aria-label="Default select example">
                            <option value="undefined" selected="true" disabled>Seleccione una terapia:</option>
                            @foreach ($terapias as $terapia)
                                <option value={{$terapia->id}}>{{$terapia->nombre_terapia}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div>
                    <div class="form-group">
                        <strong>Archivo Cupón:</strong>
                        <input type="file" name="cupon_cita" class="form-control">
                      </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection
