@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE CITAS</h1>
        <div>
            <a class="btn btn-success" href="{{ route('citas.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Fecha de Cita</th>
                <th>Hora de Cita</th>
                <th>Paciente</th>
                <th>Terapia</th>
                <th>Acciones</th>
            </tr>
            @foreach ($citas as $cita)
                <tr>
                    <td>{{ $cita->id }}</td>
                    <td>{{ $cita->fecha_cita }}</td>
                    <td>{{ $cita->hora_cita }}</td>
                    <td>{{ $cita->paciente->nombres_paciente }}</td>
                    <td>
                        @foreach ($cita->terapias as $terapia)
                            <p>{{ $terapia->nombre_terapia }}</p>
                        @endforeach
                    </td>
                    @if ($cita->cupon_cita != '')
                        <td><a href="./cupones/{{ $cita->cupon_cita }}" target="blank"><i style="color: red;"
                                    class="fas fa-file-pdf fa-2x"></i></a></td>
                    @else
                        <td></td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
@endsection
